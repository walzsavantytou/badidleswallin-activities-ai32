<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseExceptoins;

class ValFormBorrowedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patron_id' => 'required|numeric',
            'copies' => 'required|numeric',
            'book_id'=> 'required|numeric'
        ];
    }
    public function valFormMessage(){
        return[
            'patron_id.required' =>'Patron ID Required',
            'patron_id.numeric' =>'Patron ID must be Numeric',
            'copies.required' =>'Copies Field Required',
            'copies.numeric' => 'Copies Field must be Numeric',
            'book_id.required' =>'Book ID Required',
            'book_id.numeric' =>'Book ID must be Numeric'
        ];
    }
    protected function failedVal(Validator $validator){
        throw new HttpResponseException(response()->json($validator->errors(),422));
    }
}

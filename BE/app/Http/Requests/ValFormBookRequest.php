<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseExceptoins;

class ValFormBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:50',
            'author' => 'required|min:5|max:30',
            'copies' => 'required|numeric',
            'categories' => 'required|exists:categories,id'
        ];
    }
    public function valFormMessage()
    {
        return[
            'name.required'=>'Book name field is required',
            'name.min'=> 'Book name require at least 3 Characters',
            'name.max'=> 'Book name must be less than 51 Characters',
            'author.required' => 'Book Author field is required',
            'author.min' => 'Book Author require at least 5 Characters',
            'author.max' => 'Book Author must be less than 30 Characters',
            'copies.required'=> 'Book Copies field is required',
            'copies.numeric' => 'Book Copies must be Numeric',
            'categories.required' =>'Book Category not recognize',
            'categories.exist' =>'Book Category does not Exist',  
        ];
    }
    protected function failedVal(Validator $validator){
        throw new HttpResponseException(response()->json($validator->errors(),422));
    }
}
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseExceptoins;

class ValFormPatronRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => 'required|min:2|max:20',
            'first_name' => 'required|min:3|max:30',
            'middle_name' => 'required|min:2|max:30',
            'email' => 'required|min:10|max:30',
        ];
    }
    public function valFormMessage(){
        return[
            'last_name.required'=>'Last name field required',
            'last_name.min'=>'Last name Field must have at least 2 Characters',
            'last_name.max'=>'Last name Field must be less than 21 Characters',
            'first_name.required'=>'First name Field is required',
            'first_name.min'=>'First name Field must have at least 3 Characters',
            'first_name.max'=>'First name Field must be less than 31 Characters',
            'middle_name.required'=>'Middle name Field Required',
            'middle_name.min'=>'Middle name Field must have at least 2 Characters',
            'middle_name.max'=>'Middle name Field must be less than 31 Characters',
            'email.required'=>'Email Field is required',
            'email.min'=>'Email Field must have at least 10 Characters',
            'email.max'=>'Email Field must be less than 31 Characters',
        ];
    }
        protected function failedVal(Validator $validator){
            throw new HttpResponseException(response()->json($validator->errors(),422));
    }
}

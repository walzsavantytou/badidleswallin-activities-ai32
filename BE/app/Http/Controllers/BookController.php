<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Book;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValFormBookRequest;

class BookController extends Controller
{
    public function index()
    {

        return response()->json(Book::all());
    }

    public function store(ValFormBookRequest $request)
    {
        Book::create($request->validated());
        return response()->json(['message'=>'Book added successfully', 'book' => $book->with(['category:id,category'])->where('id', $book->id)->firstOrFail()], 201);
    }

    public function show($id)
    {
        return response()->json(Book::with(['category:id,category'])->where('id', $id)->firstOrFail());
    }

    public function update(Request $request, $id)
    {
        Book::where('id',$id)->update($request->all());
        $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
        $book->update($request->validated());
        return response()->json(['message' => 'Book updated Successfully', 'book' => $book->with(['category:id,category'])->where('id', $book->id)->firstOrFail()]);
    }

    public function destroy($id)
    {
        Book::where('id',$id)-delete();
        return response()->json(['message'=>'Book deleted Successfully']);
    }
}

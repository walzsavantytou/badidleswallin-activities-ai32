<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\http\Controllers\PatControler as ApiPatControler;
use App\http\Controllers\BookController as ApiBookController;
use App\http\Controllers\CatController as ApiCatController;
use App\Http\Controllers\BorrowedBook as ApiBorrowedBookController;
use App\Http\Controllers\ReturnedBook as ApiReturnedBookController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resources([
    'patrons'=>ApiPatControler::class,
    'books'=>ApiBookController::class,
    'categories'=>ApiCatController::class,
    'borrowed_books'=>ApiBorrowedBookController::class,
    'returned_books'=>ApiReturnedBookController::class
]);